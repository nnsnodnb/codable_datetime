import UIKit

struct Response: Codable {

    let date: Date

    private enum CodingKeys: String, CodingKey {
        case date
    }
}

enum Path: String {
    case `default` = "defaultJsonResponse/"
    case without = "withoutSerializer/"
    case with = "withSerializer/"
}

func request(withPath path: Path, decoder: JSONDecoder, resultHandler: @escaping (ResponseResult) -> Void) {
    let url = URL(string: "http://127.0.0.1:8000/\(path.rawValue)")!
    let tasks = URLSession.shared.dataTask(with: url) { (data, _, error) in
        guard let data = data, error == nil else {
            resultHandler(.failure(error!))
            return
        }
        do {
            let value = try decoder.decode(Response.self, from: data)
            resultHandler(.success(value))
        } catch {
            resultHandler(.failure(error))
        }
    }
    tasks.resume()
}

typealias ResponseResult = Result<Response, Error>

func defaultJsonResponse(resultHandler: @escaping (ResponseResult) -> Void) {
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .formatted({
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return formatter
    }())
    request(withPath: .default, decoder: decoder, resultHandler: resultHandler)
}

func withoutSerializerResponse(resultHandler: @escaping (ResponseResult) -> Void) {
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .formatted({
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
        return formatter
    }())
    request(withPath: .without, decoder: decoder, resultHandler: resultHandler)
}

func withSerializerResponse(resultHandler: @escaping (ResponseResult) -> Void) {
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .iso8601
    request(withPath: .with, decoder: decoder, resultHandler: resultHandler)
}

var data: [[String: Date]] = []

func debugger() {
    if data.count != 5 { return }

    data.sorted(by: { $0[$0.keys.first!]! < $1[$1.keys.first!]! })

    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
    data.forEach {
        let key = $0.keys.first!
        print(key)
        print(formatter.string(from: $0[key]!))
        print("===================")
    }
}

defaultJsonResponse {
    switch $0 {
    case .success(let value):
        data.append(["default": value.date])
        debugger()
    case .failure(let error):
        print("default: \(error)")
    }
}

withoutSerializerResponse {
    switch $0 {
    case .success(let value):
        data.append(["without": value.date])
        debugger()
    case .failure(let error):
        print("without: \(error)")
    }
}

(0...2).forEach { (index) in
    withSerializerResponse {
        switch $0 {
        case .success(let value):
            data.append(["with_\(index)": value.date])
            debugger()
        case .failure(let error):
            print("with_\(index): \(error)")
        }
    }
}
