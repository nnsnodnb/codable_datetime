from django.http.response import JsonResponse
from django.utils.timezone import now
from django.views.generic import View
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.views import APIView


class DefaultJsonResponseView(View):

    def get(self, request, *args, **kwargs):
        return JsonResponse({'date': now()})


class WithoutSerializerAPIView(APIView):

    def get(self, request, *args, **kwargs):
        return Response(data={'date': now()})


class WithSerializerAPIView(APIView):

    class Model:

        def __init__(self, date):
            self.date = date

    class ModelSerializer(serializers.Serializer):

        date = serializers.DateTimeField()

        class Meta:
            fields = ('date',)

    def get(self, request, *args, **kwargs):
        model = self.Model(date=now())
        data = self.ModelSerializer(model).data
        return Response(data=data)
