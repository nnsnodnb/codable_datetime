from django.urls import path
from .views import DefaultJsonResponseView, WithoutSerializerAPIView, WithSerializerAPIView


app_name = 'app'


urlpatterns = [
    path('defaultJsonResponse/', DefaultJsonResponseView.as_view(), name='default_json_response_view'),
    path('withoutSerializer/', WithoutSerializerAPIView.as_view(), name='without_serializer_api_view'),
    path('withSerializer/', WithSerializerAPIView.as_view(), name='with_serializer_api_view'),
]
