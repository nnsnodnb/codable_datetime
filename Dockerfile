FROM python:3.7.0
MAINTAINER nnsnodnb

RUN apt-get update
RUN pip install -U pip && pip install pipenv

COPY ./ /

WORKDIR /server

RUN pipenv lock -r > requirements.txt && \
  pip install -r requirements.txt

EXPOSE 8000
ENTRYPOINT ["python", "manage.py", "runserver", "0.0.0.0:8000"]
